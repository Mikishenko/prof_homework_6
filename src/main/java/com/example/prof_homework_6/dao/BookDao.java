package com.example.prof_homework_6.dao;

import com.example.prof_homework_6.model.Book;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

@Component
@Scope("prototype")
public class BookDao {

    public List<Book> findListBookByListTitle(List<String> listTitle) throws SQLException {

        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/db_library",
                "postgres",
                "12345")) {

            PreparedStatement preparedStatement = connection.prepareStatement(
                    "select * from books where title IN ("
                            + this.arrayToSqlInChecker(listTitle) +
                            ")");
            ResultSet resultSet = preparedStatement.executeQuery();
            List<Book> book = new ArrayList<>();

            while (resultSet.next()) {
                Book current_book = new Book();
                current_book.setId(resultSet.getLong("id"));
                current_book.setTitle(resultSet.getString("title"));
                current_book.setAuthor(resultSet.getString("author"));
                current_book.setDate_added(resultSet.getDate("date_added"));
                book.add(current_book);
            }
            return book;
        } catch (Exception e) {
            System.out.println("Connection failed");
            e.printStackTrace();
            return null;
        }
    }

    /**
     * вспомогательный метод для преобразования List<String> в String
     * для использования в команде IN в запросе к БД
     *
     * @param loc_list преобразуемое значение List<String>
     * @return String параметров через знак запятой
     */
    private String arrayToSqlInChecker(List<String> loc_list) {
        StringBuilder value = new StringBuilder("");
        for (int i = 0; i < loc_list.size(); i++) {
            value.append("'" + loc_list.get(i) + "'");
            if (i != loc_list.size() - 1) {
                value.append(",");
            }
        }
        return value.toString();
    }

}