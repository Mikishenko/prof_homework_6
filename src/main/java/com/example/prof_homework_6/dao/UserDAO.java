package com.example.prof_homework_6.dao;

import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.sql.*;
import java.util.Arrays;
import java.util.List;

@Component
@Scope("prototype")
public class UserDAO {

    /**
     * метод, который с помощью UserDAO достанет
     * из БД список названий книг человека с указанным email
     *
     * @param email почта по которой происходит отбор пользователя
     * @return список названий книг в виде List<String>
     * @throws SQLException предусмотренный тип исключения
     */
    public List<String> findListTitleBookUserByEmail(String email) throws SQLException {

        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/db_library",
                "postgres",
                "12345")) {
            PreparedStatement selectUserByEmail = connection.prepareStatement("select userbooks from user_of_library where email = ?");
            selectUserByEmail.setString(1, email);
            ResultSet resultSetUSER = selectUserByEmail.executeQuery();
            String books = null;

            while (resultSetUSER.next()) {
                books = resultSetUSER.getString("userbooks");
            }

            List<String> items = Arrays.asList(books.split("\\s*,\\s*"));

            return items;

        } catch (Exception e) {
            System.out.println("Connection failed");
            e.printStackTrace();
            return null;
        }
    }

    /**
     * метод,который позволяет добавить пользователей в базу
     *
     * @param sName     Имя пользователя
     * @param fName     Фамилия пользователя
     * @param dBirthday дата рождения
     * @param mail      адрес электронной почты
     * @param ph        телефон
     * @param uBooks    список книг, разделёных запятой
     * @throws SQLException предусмотренный тип исключения
     */
    public void addUserToDB
    (String sName,
     String fName,
     Date dBirthday,
     String mail,
     String ph,
     String uBooks) throws SQLException {
        try (Connection connection = DriverManager.getConnection(
                "jdbc:postgresql://localhost:5432/db_library",
                "postgres",
                "12345")) {

            PreparedStatement preparedStatement = connection.prepareStatement("insert into User_Of_Library " +
                    "(secondName, firstName, dateBirthday, email, phone,  userBooks)" +
                    " values (?, ?, ?, ?, ?, ?)");
            preparedStatement.setString(1, sName);
            preparedStatement.setString(2, fName);
            preparedStatement.setDate(3, dBirthday);
            preparedStatement.setString(4, mail);
            preparedStatement.setString(5, ph);
            preparedStatement.setString(6, uBooks);
            preparedStatement.execute();
        } catch (Exception e) {
            System.out.println("Connection failed");
            e.printStackTrace();

        }
    }

}
