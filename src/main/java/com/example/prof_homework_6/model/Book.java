package com.example.prof_homework_6.model;

import lombok.*;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

import java.util.Date;

@Data
public class Book {

    private Long id;
    private String title;
    private String author;
    private Date date_added;

}
