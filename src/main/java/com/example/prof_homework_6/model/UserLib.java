package com.example.prof_homework_6.model;

import lombok.*;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

@Data
public class UserLib {

    private Long id;
    private String secondName;
    private String firstName;
    private Date dateBirthday;
    private String email;
    private String phone;
    private String userBooks;
}
