package com.example.prof_homework_6;

import com.example.prof_homework_6.dao.BookDao;
import com.example.prof_homework_6.dao.UserDAO;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.sql.Date;
import java.text.DateFormat;

@SpringBootApplication
public class ProfHomeWork6Application implements CommandLineRunner {

    public static void main(String[] args) {

        SpringApplication.run(ProfHomeWork6Application.class, args);
    }

    @Override
    public void run(String... args) throws Exception {

        // отдельно прикладываю файл query.sql для дозаполнения данными БД

        AnnotationConfigApplicationContext contextUserDao = new AnnotationConfigApplicationContext(UserDAO.class);
        UserDAO userDAO = contextUserDao.getBean(UserDAO.class);

        //добавляем пользователя в БД
        userDAO.addUserToDB("Алексей", "Иванович", Date.valueOf("1957-04-24"), "mpp@yandex.ru", "+74957361539", "Колобок");

        //выводим список книг пользователя по его адресу почты
        System.out.println(userDAO.findListTitleBookUserByEmail("mamkin_sun@mail.ru"));

        AnnotationConfigApplicationContext contextBook = new AnnotationConfigApplicationContext(BookDao.class);
        BookDao bookDao = contextBook.getBean(BookDao.class);

        // выводим всю информацию о книгах, отобранных по пользователю с определённым e-mail
        System.out.println(bookDao.findListBookByListTitle(userDAO.findListTitleBookUserByEmail("mamkin_sun@mail.ru")));
    }

}