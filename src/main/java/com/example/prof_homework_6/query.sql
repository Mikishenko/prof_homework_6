--- создаём таблицы в БД

create table books
(
    id         serial
        primary key,
    title      varchar(100) not null,
    author     varchar(100) not null,
    date_added timestamp    not null
);

alter table public.books
    owner to postgres;



create table user_Of_Library
(
    id           serial primary key,
    secondName   VARCHAR(50),
    firstName    VARCHAR(50),
    email        VARCHAR(50),
    phone        VARCHAR(50),
    dateBirthday timestamp,
    userBooks    VARCHAR(255)
);


-- добавляем пользователей

insert into User_Of_Library (secondName, firstName, dateBirthday, email, phone, userBooks)
values ('Пугачёва', 'Алла', '1949-04-15', 'alla@mail.ru', '79130419504',
        'Старуха Изергиль,Путешествие к центру земли,Доктор Живаго');
insert into User_Of_Library (secondName, firstName, dateBirthday, email, phone, userBooks)
values ('Галкин', 'Максим', '1976-06-18', 'mamkin_sun@mail.ru', '+79130419501', 'Недоросль,На дне');
insert into User_Of_Library (secondName, firstName, dateBirthday, email, phone, userBooks)
values ('Кличко', 'Виталий', '1971-07-19', 'todayornottoday@mail.ru', '+39130419503', 'Колобок,Репка,Идиот');
insert into User_Of_Library (secondName, firstName, dateBirthday, email, phone, userBooks)
values ('Иванов', 'Иван', '1983-09-30', 'ivan@mail.ru', '+79160419504', 'Алхимик,Алиса в стране чудес,Буратино');


-- добавляем в существующую таблицу книги
insert into books (title, author, date_added)
values ('Алхимик', 'Паоло Коэльо', now());
insert into books (title, author, date_added)
values ('Алиса в стране чудес', 'Льюис Кэрролл', now());
insert into books (title, author, date_added)
values ('Буратино', 'А.Н. Толстой', now());
insert into books (title, author, date_added)
values ('Колобок', 'русская народная сказка', now());
insert into books (title, author, date_added)
values ('Репка', 'русская народная сказка', now());
insert into books (title, author, date_added)
values ('Идиот', 'Ф.М. Достоевский', now());
insert into books (title, author, date_added)
values ('Недоросль', 'Д.И. Фонвизин', now());
insert into books (title, author, date_added)
values ('Старуха Изергиль', 'Максим Горький', now());
insert into books (title, author, date_added)
values ('На дне', 'Максим Горький', now());
insert into books (title, author, date_added)
values ('Путешествие к центру земли', 'Жюль Верн', now());


-- список юзеров после работы метода добавляения
select *
from user_Of_Library;

